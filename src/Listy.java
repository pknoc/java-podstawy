import java.util.ArrayList;
import java.util.List;

public class Listy {

    public static void main(String[] args) {

        List<Integer> lista= new ArrayList<Integer>();
        lista.add(1);
        lista.add(2);
        lista.add(4);
        lista.add(15);
        lista.add(26);

        System.out.println(lista.size());


        for(Integer elementListy: lista){
            System.out.println(elementListy);
        }

        lista.remove(new Integer(4));

        System.out.println("");

        for(Integer elementListy: lista){
            System.out.println(elementListy);
        }


        System.out.println(lista.size());
        System.out.println(lista.get(3));

    }
}
