import java.util.Random;

public class Tabelki {

    public static void main(String[] args) {
/*
        typ[] nazwa tablicy = new typ[liczba elementow];
        typ[] nazwa tablicy = new typ[]{element1, element2...};
        String[] x  = new String[4];
        x[2] = "xyz";
        System.out.println(x[2]);
 */
        Random r = new Random();
        int t = 100;

        int y = 6;
        int[] x  = new int[y];
        for(int o = 0; o < y; o++){
            x[o] = r.nextInt(t);
            System.out.println(x[o]);
        }

        System.out.println("Petla 2");

        int tb2l = 9;
        String[] tb2 = new String[tb2l];
        int i = 0;
        while (i < tb2l) {
        if(i == 5){
            System.out.println("Element " + (i+1) + " zostal pominiety.");
            i++;
            continue;
        }
            System.out.println(tb2[i]);
            i++;
        }

    }
}
